﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //Components
    private Transform transform;
    private Arm arm;

    //Values
    [SerializeField]
    private float speed;
    [SerializeField]
    private float rotationSpeed;


    void Start() {
        transform = GetComponent<Transform>();
        arm = GetComponentInChildren<Arm>();
	}
	
	void Update () {
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            transform.Rotate(transform.up * rotationSpeed * Time.deltaTime);
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            transform.Rotate(transform.up * -rotationSpeed * Time.deltaTime);
        }

        if (Input.GetAxisRaw("Vertical") > 0)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        else if (Input.GetAxisRaw("Vertical") < 0)
        {
            transform.Translate(-Vector3.forward * speed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            transform.LookAt(GetClosestEnemy());
            arm.ReadyGun();
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            arm.PutGunDown();
        }
    }

    private Vector3 GetClosestEnemy()
    {
        int closestEnemyIndex = 0;
        float closestEnemyDistance;

        GameObject[] enemyList = GameObject.FindGameObjectsWithTag("Enemy");
        closestEnemyDistance = Vector3.Distance(enemyList[0].GetComponent<Transform>().position, transform.position);

        for (int i = 0; i < enemyList.Length; i++)
        {
            if (Vector3.Distance(enemyList[i].GetComponent<Transform>().position, transform.position) < closestEnemyDistance)
                closestEnemyIndex = i;
        }

        return enemyList[closestEnemyIndex].GetComponent<Transform>().position;
    }
}
