﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arm : MonoBehaviour {
    //Components
    private Transform transform;
	void Start () {
        transform = GetComponent<Transform>();
	}

	void Update () {
		
	}

    public void ReadyGun()
    {
        transform.Rotate(Vector3.right * -45);
    }
    public void PutGunDown()
    {
        transform.Rotate(Vector3.right * 45);
    }
}
